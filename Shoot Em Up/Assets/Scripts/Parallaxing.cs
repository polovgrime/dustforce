﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class Parallaxing : MonoBehaviour
{
    [SerializeField] private float _speed = 1f;
    private MeshRenderer _meshRenderer;
    private Material _material;
    void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _material = _meshRenderer.material;
    }

    void Update()
    {
        _material.mainTextureOffset += Vector2.up * Time.deltaTime * _speed;
    }
}
