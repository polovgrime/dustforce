﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Sound
{
    public class BGmusic : MonoBehaviour
    {
        public static BGmusic instance = null;
        [SerializeField] List<AudioSource> bgtracks;
        [SerializeField] bool playOnDefault = false;
        bool isMusicOn;
        int currentTrack;
        // Start is called before the first frame update
        public bool GetMusicCondition()
        {
            return isMusicOn;
        }
        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        private void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }

        void Start()
        {
            if (!playOnDefault)
            {
                isMusicOn = false;
            }
            StartCoroutine(WaitForNextTrack());
            DontDestroyOnLoad(gameObject);

        }

        IEnumerator WaitForNextTrack()
        {
            bgtracks[currentTrack].Play();
            if(!playOnDefault)
                bgtracks[currentTrack].Pause();
            else
                isMusicOn = true;
            yield return new WaitUntil(() => !bgtracks[currentTrack].isPlaying && isMusicOn);
            currentTrack++;
            if (currentTrack >= bgtracks.Count)
                currentTrack = 0;
            
            StartCoroutine(WaitForNextTrack());

        }

        public bool OnMusicButtonClick()
        {
            if (isMusicOn)
            {
                isMusicOn = false;
                bgtracks[currentTrack].Pause();
            }
            else
            {
                isMusicOn = true;
                if (!playOnDefault)
                    playOnDefault = true;
                bgtracks[currentTrack].UnPause();
            }
            return isMusicOn;

        }
    }

}