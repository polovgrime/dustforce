﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Sound
{

    public class Sounds : MonoBehaviour
    {
        public static bool IsEnabled { private set; get; }

        public void ToggleSound()
        {
            IsEnabled = !IsEnabled;
            PlayerPrefs.SetInt("SoundsCondition", IsEnabled ? 1 : 0);
        }

        private void Start()
        {
            IsEnabled = PlayerPrefs.GetInt("SoundsCondition") > 0;
            IsEnabled = true;
        }
        
    }

}