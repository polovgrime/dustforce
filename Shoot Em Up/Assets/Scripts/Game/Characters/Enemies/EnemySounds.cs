﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySounds : MonoBehaviour
{
    [SerializeField] private List<AudioClip> _clips;
    private AudioSource _source;
    // Start is called before the first frame update
    void Start()
    {
        var currentTransform = transform;
        while (_source == null)
        {
            if (currentTransform == null) throw new Exception("AudioSource wasn't found");
            _source = currentTransform.GetComponent<AudioSource>();
            currentTransform = transform.parent;
        }
    }

    public void PlaySound()
    {
        _source.PlayOneShot(_clips[Random.Range(0, _clips.Count)]);
    }
}
