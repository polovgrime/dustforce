﻿using Game.Gameplay.Skills;
using Game.Gameplay.Turrets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay
{

    public class Boss : EnemyBase
    {
        public static event BossEventHandler BossDied;
        public static Boss instance = null;

        private List<Stage> _stages;
        private int _currentStage = 0;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        private void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }

        protected override void Start()
        {
            base.Start();
            CollectMoves();
        }

        private void CollectMoves()
        {
            _stages = new List<Stage>();
            foreach (Transform child in transform)
            {
                var skill = child.gameObject.GetComponent<Stage>();
                if (skill != null)
                    _stages.Add(skill);
            }
        }

        private void ActivateStage()
        {
            if (_currentStage < _stages.Count)
            {
                health = _stages[_currentStage].GetHealth();
                _stages[_currentStage].ActivateStage();
                _stages[_currentStage].Stopped += OnMoveStopped;
                GetComponent<CircleCollider2D>().enabled = true;
            }
            else
            {
                Destroy(gameObject);
                BossDied?.Invoke(); 
            }
        }
        
        public override void ReceiveDamage(int damage)
        {
            base.ReceiveDamage(damage);
            if (health <= 0)
            {
                if (_stages.Count > 0 && _currentStage < _stages.Count)
                {
                    health = 1000;
                    GetComponent<CircleCollider2D>().enabled = false;
                    _stages[_currentStage].InterruptStage();
                }
            }
        }

        protected override void OnMoveStopped()
        {
            if (gameObject.activeSelf)
            {
                _stages[_currentStage].Stopped -= OnMoveStopped;
                _currentStage++;
                ActivateStage();
            }
        }

        protected override void OnReachedPlayground()
        {
            Invoke("ActivateStage", 1f);
        }
    }

}