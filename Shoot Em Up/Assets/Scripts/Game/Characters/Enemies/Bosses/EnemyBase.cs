﻿using Game.Gameplay.Turrets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Gameplay.Skills;
using Game.Gameplay.Visuals;
using Game.Sound;

namespace Game.Gameplay
{
    public enum BlowType
    {
        BlowsBullets, DoesntBlow
    }
    public delegate void BossEventHandler();
    public abstract class EnemyBase : MonoBehaviour, IDamageable, IMovableBySkill, IRotatableByAction
    {
        [SerializeField] private int ramDamage;
        [SerializeField] protected string enemyName;
        [SerializeField] protected float health;
        [SerializeField] protected float _moveUsageDelay;
        [SerializeField] private ParticleSystem _deathParticles;
        [SerializeField] private ParticleSystem _damageParticles;
        protected Rigidbody2D _rgbd;
        private bool _movingDown = true;
        private EnemySounds _sounds;
        protected virtual void Start()
        {
            _rgbd = GetComponent<Rigidbody2D>();
            _sounds = GetComponent<EnemySounds>();
        }

        protected abstract void OnMoveStopped();

        protected abstract void OnReachedPlayground();

        public void MovePosition(Vector2 position)
        {
            var rgbd = gameObject.GetComponent<Rigidbody2D>();
            if (rgbd != null)
                rgbd.MovePosition(position);
        }

        public void Rotate(Vector3 eul)
        {
            transform.rotation = Quaternion.Euler(eul);
        }

        public virtual void ReceiveDamage(int damage)
        {
            health -= damage;
            var particles = health <= 0 ? _deathParticles : _damageParticles;
            var ps = Instantiate(particles, transform.position, Quaternion.identity, EnemyParticleHolder.instance.transform);
            ps.Play();
            if (Sounds.IsEnabled)
                _sounds.PlaySound();
        }


        private void OnCollisionEnter2D(Collision2D collision)
        {
            var target = collision.gameObject.GetComponent<IDamageable>();
            if (target != null)
            {
                target.ReceiveDamage(ramDamage);
                ReceiveDamage(ramDamage/2);
            }
        }

        private void FixedUpdate()
        {
            if (_movingDown)
                _rgbd.MovePosition(Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, 3.5f), Time.fixedDeltaTime));
        }

        private void Update()
        {
            if(_movingDown && transform.position.y <= 6f)
            {
                _movingDown = false;
                OnReachedPlayground();
            }
        }
    }
}