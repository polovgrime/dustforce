﻿using Game.Gameplay.Skills;
using Game.Gameplay.Visuals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay
{
    public class RegularEnemy : EnemyBase
    {
        public static event BossEventHandler EnemyEliminated;
        private List<EnemyAction> _enemyActions;
        private int _moveIteration = 0;
        private void CollectMoves()
        {
            _enemyActions = new List<EnemyAction>();
            foreach (Transform child in transform)
            {
                var skill = child.gameObject.GetComponent<EnemyAction>();
                if (skill != null)
                    _enemyActions.Add(skill);
            }
        }

        private void PrepareMoves()
        {
            foreach (var move in _enemyActions)
            {
                move.Stopped += OnMoveStopped;
            }
        }
       
        private void ActivateMove()
        {
            if (_moveIteration == _enemyActions.Count)
                _moveIteration = 0;
            var skill = _enemyActions[_moveIteration++];
            
            if (skill != null)
            {
                skill.Activate();
            }

        }

        protected override void OnMoveStopped()
        {
            if (gameObject.activeSelf)
                Invoke("ActivateMove", _moveUsageDelay);

        }

        public override void ReceiveDamage(int damage)
        {
            base.ReceiveDamage(damage);
            
            if (health <= 0)
            {
                EnemyEliminated?.Invoke();
                Destroy(gameObject);
            }
        }


        private void Activate()
        {
            _moveIteration = 0;
            Invoke("ActivateMove", .15f);
        }

        private void OnDisable()
        {
            Destroy(gameObject);
        }

        protected override void Start()
        {
            
            base.Start();
            CollectMoves();
            PrepareMoves();
        }

        protected override void OnReachedPlayground()
        {
            Activate();
        }
    }
}
