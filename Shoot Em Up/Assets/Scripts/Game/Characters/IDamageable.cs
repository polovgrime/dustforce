﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay
{ 
    public interface IDamageable
    {
        void ReceiveDamage(int damage);
    }
}