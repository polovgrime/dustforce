﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Game.Sound;

namespace Game.Gameplay
{
    public delegate void PlayerConditionChange();

    public class Player : MonoBehaviour, IDamageable
    {
        public event PlayerConditionChange PlayerDied;
        public static Player instance { get; set; }

        private AudioSource _audioSource;
        [SerializeField] private int _health = 0;
        private bool _canReceiveDamage = true;
        private CameraShake _shake;
        private void Awake()
        {
            if (instance == null) instance = this;
            else Destroy(gameObject);
        }

        private void OnDestroy()
        {
            if (instance == this) instance = null;   
        }
        private void Start()
        {
            _shake = Camera.main.gameObject.GetComponent<CameraShake>();
            _audioSource = GetComponent<AudioSource>();
        }


        private void OnEnable()
        {
            _canReceiveDamage = true;
        }

        public void ReceiveDamage(int damage)
        {
            if (_canReceiveDamage)
            {
                _health -= damage;
                if (Sounds.IsEnabled)
                    _audioSource.Play();
                
                _shake.AddStress(.6f);
                if (_health <= 0)
                {
                    gameObject.SetActive(false);
                    PlayerDied?.Invoke();
                }
            }
        }        
        
    }

}