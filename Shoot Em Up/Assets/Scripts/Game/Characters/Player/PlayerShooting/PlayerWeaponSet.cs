﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Game.Gameplay.Weapons
{
    public class PlayerWeaponSet : MonoBehaviour
    {
        private List<PlayerWeapon> _weapons;
        private int _weaponLevel = 1;
        private int _rateLevel = 1;
        private int _damageLevel = 1;

        void Start()
        {
            _weapons = transform
                .Cast<Transform>()
                .Select(e => e.gameObject.GetComponent<PlayerWeapon>())
                .Where(e => e != null)
                .ToList();

            foreach (var weapon in _weapons.Where(e => e.CheckWeaponLevel(_weaponLevel)))
            {
                weapon.ApplyDamageLevel(_damageLevel);
                weapon.ApplyRateLevel(_rateLevel);
            }

            foreach (var weapon in _weapons.Where(e => !e.CheckWeaponLevel(_weaponLevel)))
            {
                weapon.gameObject.SetActive(false);
            }
        }
    }
}

