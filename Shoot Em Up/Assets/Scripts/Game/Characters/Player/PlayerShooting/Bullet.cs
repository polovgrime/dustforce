﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Weapons
{
    class Bullet : MonoBehaviour
    {
        const float speed = 10;
        private int _damage = 0;
        // Update is called once per frame
        void Update()
        {
            transform.position += (Vector3)Vector2.up * speed * Time.deltaTime;
        }

        public void SetDamage(int damage)
        {
            _damage = damage;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            IDamageable target = collision.gameObject.GetComponent<IDamageable>();
            if (target != null)
            {
                target.ReceiveDamage(_damage);
            }
            Destroy(gameObject);
        }
    }
}

