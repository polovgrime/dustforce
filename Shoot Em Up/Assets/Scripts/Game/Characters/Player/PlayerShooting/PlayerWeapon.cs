﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Weapons
{
    public class PlayerWeapon : MonoBehaviour
    {
        [SerializeField] private int _weaponLevelRequired;
        [SerializeField] private float _baseFireRate = 1;
        [SerializeField] private int _baseDamage = 1;
        [SerializeField] private Bullet _bullet;
        private float _fireRate = 10;
        private int _damage = 2;
        private float _nextShotTime = 0;
        public bool CheckWeaponLevel(int weaponLevel)
        {
            return weaponLevel >= _weaponLevelRequired;
        }

        public void ApplyRateLevel(int fireRateLevel){
            _fireRate = fireRateLevel * _baseFireRate;
        }

        public void ApplyDamageLevel(int damageLevel){
            _damage = damageLevel * _baseDamage;
        }

        private void Fire()
        {
            var bullet = Instantiate(_bullet, transform.position, Quaternion.identity);
            bullet.SetDamage(_damage);
        }

        private void FixedUpdate()
        {
            if (Time.time > _nextShotTime)
            {
                Fire();
                _nextShotTime = Time.time + 1 / _fireRate;
            }
        }
    }
}
