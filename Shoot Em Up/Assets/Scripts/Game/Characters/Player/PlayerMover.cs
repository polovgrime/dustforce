﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] float _speed;
        private Rigidbody2D _rb;
        private Vector3 _newPosition;
        private void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            _newPosition = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));            
        }

        private void FixedUpdate()
        {
            _rb.MovePosition(transform.position + _newPosition * Time.fixedDeltaTime * _speed);
        }
    }

}