﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gameplay.Turrets
{
    public class Turret : MonoBehaviour
    {
        [SerializeField] protected float bulletVelocity;
        [SerializeField] protected int damage;
        [SerializeField] Bullet bullet;

        public void Fire()
        {
            Instantiate(bullet, transform.position, transform.rotation, transform);
        }
        public float GetBulletVelocity()
        {
            return bulletVelocity;
        }
        public virtual int GetBulletDamage()
        {
            return damage;
        }
    }
}