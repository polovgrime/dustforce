﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Gameplay.Turrets;  
namespace Game.Gameplay
{
    public class Bullet : MonoBehaviour
    {
        private int damage;
        private Turret Turret;
        private float speed;
        private void Start()
        {
            Turret = transform.parent.GetComponent<Turret>();
            speed = Turret.GetBulletVelocity();
            damage = Turret.GetBulletDamage();
            
            transform.parent = BulletField.GetFieldTransform();
        }

        private void FixedUpdate()
        {
            transform.Translate(new Vector3(0, speed / 2.5f * Time.fixedDeltaTime));
            if (Mathf.Abs(transform.position.y) > 5 || Mathf.Abs(transform.position.x) > 5)
                Destroy(gameObject);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {  
            IDamageable target = collision.gameObject.GetComponent<IDamageable>();
            if (target != null)
            {
                target.ReceiveDamage(damage);
                Destroy(gameObject);
            }
        }
    }
}
