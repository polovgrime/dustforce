﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class Stage : MonoBehaviour
    {
        public event AttackCondition Stopped;


        List<EnemyAction> _bindedActions;
        [SerializeField] float actionDelay;
        [SerializeField] int stageHealth;
        [SerializeField] private bool _isLooped = true;
        int currentActionIndex = 0;

        void ActivateCurrentAction()
        {

            _bindedActions[currentActionIndex++].Activate();
            if (currentActionIndex == _bindedActions.Count && _isLooped)
                currentActionIndex = 0;
        }
        public int GetHealth()
        {
            return stageHealth;
        }

        void CollectActions()
        {
            _bindedActions = new List<EnemyAction>();
            foreach (Transform child in transform)
            {
                var act = child.gameObject.GetComponent<EnemyAction>();
                if (act != null)
                {
                    act.Stopped += OnActionStopped;
                    _bindedActions.Add(act);
                }
            }
        }

        void OnActionStopped()
        {
            if (currentActionIndex < _bindedActions.Count)
            {
                Invoke("ActivateCurrentAction", actionDelay);
            }
        }

        public void InterruptStage()
        {
            Stopped();
            Destroy(gameObject);

        }

        // Start is called before the first frame update
        void Awake()
        {
            CollectActions();
        }

        public void ActivateStage()
        {
            currentActionIndex = 0;
            ActivateCurrentAction();
        }
    }

}