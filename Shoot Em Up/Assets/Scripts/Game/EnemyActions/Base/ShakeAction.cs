﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class ShakeAction :  EnemyAction
    {
        [SerializeField] float stress;
        [SerializeField] bool useOnlyOnce = true;
        bool skip = false;

        public override void Activate()
        {
            if (!skip)
            {
                Camera.main.gameObject.GetComponent<CameraShake>().AddStress(stress);
                if (useOnlyOnce)
                    skip = true;
            }
            StopCurrentAction();
        }
    }

}