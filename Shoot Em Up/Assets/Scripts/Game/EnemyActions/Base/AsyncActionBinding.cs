﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class AsyncActionBinding : EnemyAction
    {
        private List<EnemyAction> _bindedActions;
        private int _actionsStopped = 0;
        
        public override void Activate()
        {
            _actionsStopped = 0;
            ActivateActions();
        }

        private void ActivateActions()
        {
            foreach (var skill in _bindedActions)
            {
                skill.Activate();
            }
        }

        private void OnActionStopped()
        {
            _actionsStopped++;
            if (_actionsStopped == _bindedActions.Count)
            {
                StopCurrentAction();
            }
        }

        private void CollectActions()
        {
            _bindedActions = new List<EnemyAction>();
            foreach(Transform child in transform)
            {
                var act = child.gameObject.GetComponent<EnemyAction>();
                if (act != null)
                {
                    _bindedActions.Add(act);
                    act.Stopped += OnActionStopped;
                }
            }
        }

        private void Start()
        {
            CollectActions();            
        }
    }
}
