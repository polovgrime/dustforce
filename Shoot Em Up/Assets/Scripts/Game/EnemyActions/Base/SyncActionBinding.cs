﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills.Stages
{

    public class SyncActionBinding : EnemyAction
    {
        [SerializeField] private float _actionDelay;
        private List<EnemyAction> _bindedActions;
        private int _currentActionIndex = 0;

        public override void Activate()
        {
            _currentActionIndex = 0;
            ActivateCurrentAction();
        }

        private void Start()
        {
            CollectActions();
        }

        private void CollectActions()
        {
            _bindedActions = new List<EnemyAction>();
            foreach (Transform child in transform)
            {
                var act = child.gameObject.GetComponent<EnemyAction>();
                if (act  != null)
                {
                    act.Stopped += OnActionStopped;
                    _bindedActions.Add(act);
                }
            }
        }

        private void OnActionStopped()
        {
            _currentActionIndex++;
            if (_currentActionIndex == _bindedActions.Count)
            {
                _currentActionIndex = 0;
                StopCurrentAction();
            } 
            else
            {
                Invoke("ActivateCurrentAction", _actionDelay);
            }
        }

        private void ActivateCurrentAction()
        {
            _bindedActions[_currentActionIndex].Activate();
        }
    }

}