﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills.Sta
{

    public class EndStage : EnemyAction
    {
        [SerializeField] int _damageToBoss = 2000;
        public override void Activate()
        {
            if (Boss.instance != null)
            {
                Boss.instance.ReceiveDamage(_damageToBoss);
                StopCurrentAction();
            }
        }
    }

}