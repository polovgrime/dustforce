﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{
    public class MoveToRandomHor : MovementAction
    {
        [SerializeField] float speed;
        public override void Activate()
        {
            StartCoroutine(MoveToPoint());
        }

        IEnumerator MoveToPoint()
        {
            var halfWidth = Camera.main.orthographicSize * Camera.main.aspect;
            var halfHeight = Camera.main.orthographicSize;
            var randomPoint = new Vector2(Random.Range(-halfWidth + 0.5f, halfWidth - 0.5f), transform.position.y);
            while (Vector2.Distance((Vector2)transform.parent.position, randomPoint) > 0.1)
            {
                var newPos = Vector2.MoveTowards(transform.parent.position, randomPoint, 2 * speed * Time.fixedDeltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
                
            }
            StopCurrentAction();
        }

        protected override void Start()
        {
            base.Start();
        }
    }


}