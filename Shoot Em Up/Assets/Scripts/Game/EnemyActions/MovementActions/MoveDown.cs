﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class MoveDown : MovementAction
    {
        [SerializeField] private float _yPoint;
        [SerializeField] private float _speed;
        private Vector2 _point;

        public override void Activate()
        {
            StartCoroutine(MoveToPoint());
        }

        IEnumerator MoveToPoint()
        {
            _point = new Vector2(transform.position.x, _yPoint);

            while (Vector2.Distance(transform.parent.position, _point) > 0.1)
            {
                var newPos = Vector2.MoveTowards(transform.parent.position, _point, 2 * _speed * Time.fixedDeltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
            }
            StopCurrentAction();
        }

        protected override void Start()
        {
            base.Start();
        }
    }
}
