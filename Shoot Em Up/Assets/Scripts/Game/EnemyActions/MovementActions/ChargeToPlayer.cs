﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gameplay.Skills
{

    public class ChargeToPlayer : MovementAction
    {
        
        [SerializeField] private float _speed;
        [SerializeField] private bool _shouldRefresh = false;
        private Vector3 _playerPosisition;
        private Vector3 _direction;

        public override void Activate()
        {
            StartCoroutine(Charge());
        }

        private void FindPlayer()
        {
            if (Player.instance != null)
                _playerPosisition = Player.instance.transform.position;
            var heading = transform.position - _playerPosisition;
            var distance = heading.magnitude;
            _direction = heading / distance;
        }
       
        IEnumerator Charge()
        {
            FindPlayer();
            while (Vector2.Distance(transform.position, _playerPosisition * 10) > 0.025)
            {
                Vector2 newPos = Vector2.MoveTowards(transform.position, transform.position - _direction, _speed * Time.fixedDeltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}

