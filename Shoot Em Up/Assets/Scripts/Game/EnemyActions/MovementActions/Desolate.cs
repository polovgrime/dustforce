﻿
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class Desolate : EnemyAction
    {
        public override void Activate()
        {
            transform.parent.gameObject.SetActive(false);
        }
    }

}