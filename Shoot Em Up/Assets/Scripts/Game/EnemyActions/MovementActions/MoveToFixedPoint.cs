﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class MoveToFixedPoint : MovementAction
    {
        [SerializeField] Vector2 point;
        [SerializeField] float speed;
        public override void Activate()
        {
            StartCoroutine(MoveToPoint());
        }

        IEnumerator MoveToPoint()
        {
            while (Vector2.Distance(transform.parent.position, point) > 0.1)
            {
                Vector2 newPos = Vector2.MoveTowards(transform.parent.position, point, 2 * speed * Time.fixedDeltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
            }
            StopCurrentAction();
        }

        protected override void Start()
        {
            base.Start();
        }
    }

}