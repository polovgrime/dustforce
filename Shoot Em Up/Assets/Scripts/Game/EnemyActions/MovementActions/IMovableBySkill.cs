﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{
    public interface IMovableBySkill
    {
        void MovePosition(Vector2 position);
    }
}

