﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gameplay.Skills
{
    public class Charge : MovementAction
    {
        [SerializeField] private float _speed;
        
        protected override void Start()
        {
            base.Start();
        }

        public override void Activate()
        {
            StartCoroutine(MoveDown(-0.5f));
        }

        IEnumerator MoveDown(float yPosition)
        {
            
            while (Vector2.Distance((Vector2)transform.parent.position, new Vector2(transform.parent.position.x, yPosition)) > 0.1)
            {
                Vector2 newPos = Vector2.MoveTowards(transform.parent.position, new Vector2(transform.parent.position.x, yPosition), 2 * _speed * Time.deltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
            }
            StartCoroutine(MoveUp(2));
        }

        IEnumerator MoveUp(float yPosition)
        {
            while (Vector2.Distance(transform.position, new Vector2(transform.position.x, yPosition)) > 0.1)
            {
                Vector2 newPos = Vector2.MoveTowards(transform.parent.position, new Vector2(transform.parent.position.x, yPosition), 2 * _speed * Time.deltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
            }
            StopCurrentAction();
        }
    }
}
