﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class ScreenBasedMovement : MovementAction
    {
        [SerializeField] Vector2 _point = Vector2.down;
        [SerializeField] float _speed = 2;
        [SerializeField] float _yOffset = -1;
        public override void Activate()
        {
            StartCoroutine(MoveToPoint());
        }


        IEnumerator MoveToPoint()
        {
            while (Vector2.Distance((Vector2)transform.parent.position, _point) > 0.1)
            {
                Vector2 newPos = Vector2.MoveTowards(transform.parent.position, _point, 2 * _speed * Time.fixedDeltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
            }
            StopCurrentAction();
        }

        protected override void Start()
        {
            base.Start();
            var halfWidth = Camera.main.orthographicSize * Camera.main.aspect;
            var halfHeight = Camera.main.orthographicSize;
            var widthPercent = halfWidth / 100;
            var heightPercent = halfHeight / 100;
            _point = new Vector2(_point.x * widthPercent, _point.y * heightPercent - _yOffset);
        }
    }

}