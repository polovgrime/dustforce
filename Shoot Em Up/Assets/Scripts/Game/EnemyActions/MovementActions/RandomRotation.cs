﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{
    public class RandomRotation : RotateAction
    {
        [SerializeField] float maxAngle = 180;
        [SerializeField] float minAngle = -180; 
        public override void Activate()
        {
            SetRotation();
        }

        void SetRotation()
        {
            var euler = new Vector3(0, 0, Random.Range(minAngle, maxAngle));
            _rotateParent.Rotate(euler);
            StopCurrentAction();
        }

        protected override void Start()
        {
            base.Start();
        }
    }

}
