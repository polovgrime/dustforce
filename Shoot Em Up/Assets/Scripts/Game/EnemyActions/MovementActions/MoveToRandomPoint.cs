﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class MoveToRandomPoint : MovementAction
    {

        [SerializeField] float distanceFromBottom;
        [SerializeField] float speed;

        public override void Activate()
        {
            StartCoroutine(MoveToPoint());;
        }

        IEnumerator MoveToPoint()
        {
            var halfWidth = Camera.main.orthographicSize * Camera.main.aspect;
            var halfHeight = Camera.main.orthographicSize;
            var randomPoint = new Vector2(Random.Range(-halfWidth + 0.5f, halfWidth - 0.5f), Random.Range(distanceFromBottom - 1, halfHeight -.25f));
            while (Vector2.Distance((Vector2)transform.parent.position, randomPoint) > 0.1)
            {
                var newPos = Vector2.MoveTowards(transform.parent.position, randomPoint, 2 * speed * Time.fixedDeltaTime);
                _parentMover.MovePosition(newPos);
                yield return new WaitForFixedUpdate();
            }
            StopCurrentAction();
        }

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
        }
    }

}