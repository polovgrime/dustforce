﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{
    public interface IRotatableByAction
    {
        void Rotate(Vector3 position);
    }
}
