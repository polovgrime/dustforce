﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField] float trauma = 0;
        [SerializeField] float stress;
        [SerializeField] float decreaseSpeed = 1;
        [SerializeField] bool test = false;
        [SerializeField] float maxOffsetY = .5f;
        [SerializeField] float maxOffsetX = .5f;    
        [SerializeField] float maxAngle = 30;
        float offsetY;
        float offsetX;
        float angle;
        Vector3 defaultCameraPos;
        Vector3 defaultCameraEul;
        // Start is called before the first frame update
        void Start()
        {
            defaultCameraEul = Camera.main.transform.eulerAngles;
            defaultCameraPos = Camera.main.transform.position;
        }

        public void AddStress(float stress)
        {
            trauma += stress;
        }

        // Update is called once per frame
        void Update()
        {
            if (test)
            {
                trauma += stress;
                test = false;
            }
            if (trauma > 0)
            {
                CalculateOffset();
                Camera.main.transform.eulerAngles = defaultCameraEul + new Vector3(0, 0, angle);
                Camera.main.transform.position = defaultCameraPos + new Vector3(offsetX, offsetY);
                trauma -= Time.deltaTime * decreaseSpeed;
                if (trauma <= 0)
                {
                    trauma = 0;
                    Camera.main.transform.eulerAngles = defaultCameraEul;
                    Camera.main.transform.position = defaultCameraPos;
                }
            }

        }

        void CalculateOffset()
        {

            offsetY = maxOffsetY * Mathf.Pow(trauma, 2) * Random.Range(-1f, 1f);
            offsetX = maxOffsetX * Mathf.Pow(trauma, 2) * Random.Range(-1f, 1f);
            angle = maxAngle * Mathf.Pow(trauma, 2) * Random.Range(-1f, 1f);
        }
    }
}
