﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gameplay.Skills
{
    public delegate void AttackCondition();

    public abstract class EnemyAction : MonoBehaviour
    {
        public event AttackCondition Stopped;
        public abstract void Activate();
        protected void StopCurrentAction()
        {
            Stopped();
        }
    }
}
