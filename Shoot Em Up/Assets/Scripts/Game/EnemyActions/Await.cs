﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gameplay.Skills
{
    public class Await : EnemyAction
    {
        [SerializeField] float timeAwait;
        public override void Activate()
        {
            StartCoroutine(StartAwaiting());  
        }

        IEnumerator StartAwaiting()
        {
            yield return new WaitForSeconds(timeAwait);
            Deactivate();
        }

        private void Deactivate() 
        {
            StopCurrentAction();
        }

    }
}