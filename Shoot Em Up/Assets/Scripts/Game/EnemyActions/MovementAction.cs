﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{
    public abstract class MovementAction : EnemyAction
    {
        protected IMovableBySkill _parentMover;

        protected virtual void Start()
        {
            _parentMover = GetMovingParent();
        }

        private IMovableBySkill GetMovingParent()
        {
            IMovableBySkill movingObject = null;
            Transform currentTransform = transform;
            
            while (movingObject == null)
            {
                if (currentTransform == null) throw new System.Exception("No moving parent found!");
                movingObject = currentTransform.GetComponent<IMovableBySkill>();
                currentTransform = currentTransform.parent;
            }

            return movingObject;
        }
    }
}

