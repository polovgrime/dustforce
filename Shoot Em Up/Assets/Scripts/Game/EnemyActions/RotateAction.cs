﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game.Gameplay.Skills
{
    public abstract class RotateAction : EnemyAction
    {
        protected IRotatableByAction _rotateParent;

        protected virtual void Start()
        {
            _rotateParent = GetRotatingParent();
        }

        private IRotatableByAction GetRotatingParent()
        {
            IRotatableByAction movingObject = null;
            Transform currentTransform = transform;

            while (movingObject == null)
            {
                if (currentTransform == null) throw new System.Exception("No rotating parent found!");
                movingObject = currentTransform.GetComponent<IRotatableByAction>();
                currentTransform = currentTransform.parent;
            }

            return movingObject;
        }
    }
}