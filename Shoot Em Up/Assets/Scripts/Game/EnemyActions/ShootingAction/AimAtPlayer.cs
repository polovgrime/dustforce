﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gameplay.Skills
{
    public class AimAtPlayer : RotateAction
    {
        public override void Activate()
        {
            Aim();
        }

        private void Aim()
        {
            var diff = Player.instance.transform.position - transform.position;
            var rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            _rotateParent.Rotate(new Vector3(0f, 0f, rot_z + 90));
            StopCurrentAction();
        }

        protected override void Start()
        {
            base.Start();
        }
    }
}