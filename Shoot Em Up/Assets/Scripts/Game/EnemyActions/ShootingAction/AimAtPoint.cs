﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class AimAtPoint : RotateAction
    {
        [SerializeField] Vector2 point;
        [SerializeField] float yOffset = -1;
        public override void Activate()
        {
            Aim();
        }

        private void Aim()
        {
            Vector2 diff = point - (Vector2)transform.position;
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            _rotateParent.Rotate(new Vector3(0f, 0f, rot_z + 90));
            StopCurrentAction();
        }

        protected override void Start()
        {
            base.Start();
            var halfWidth = Camera.main.orthographicSize * Camera.main.aspect;
            var halfHeight = Camera.main.orthographicSize;
            var widthPercent = halfWidth / 100;
            var heightPercent = halfHeight / 100;
            point = new Vector2(point.x * widthPercent, point.y * heightPercent - yOffset);
        }
    }
}