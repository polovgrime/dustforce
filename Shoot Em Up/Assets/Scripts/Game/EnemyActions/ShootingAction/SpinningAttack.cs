﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Gameplay.Turrets;
namespace Game.Gameplay.Skills
{

    public class SpinningAttack : EnemyAction
    {
        [SerializeField] float zAngleOffset;
        [SerializeField] float time;
        [SerializeField] float attackDelay;

        private Vector3 _angleOffset;
        private List<Turret> _simpleTurrets;

        private void Start()
        {
            CollectTurrets();
            _angleOffset = new Vector3(0, 0, zAngleOffset);
        }

        public override void Activate()
        {
            StartCoroutine(StartSpinning());
        }

        private void CollectTurrets()
        {
            _simpleTurrets = new List<Turret>();
            foreach (Transform child in transform)
            {
                var tur = child.GetComponent<Turret>();
                if (tur != null)
                {
                    _simpleTurrets.Add(tur);
                }
            }
        }

        IEnumerator StartSpinning()
        {
            while (time > 0)
            {
                foreach (var tur in _simpleTurrets)
                {
                    tur.Fire();
                }
                transform.eulerAngles += _angleOffset;
                yield return new WaitForSeconds(attackDelay);
                time -= attackDelay;
            }
            StopCurrentAction();
        }
    }
}
