﻿using Game.Gameplay.Turrets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{
    public class ClockLikeAttack : EnemyAction
    {
        [SerializeField] private int attackCount;
        [SerializeField] private float timeAttackDelay;
        [SerializeField] private float zAngleOffset;
        private Vector3 _angleOffset;
        private List<Turret> _simpleTurrets;
        public override void Activate()
        {
            StartCoroutine(StageFire());
        }

        private void CollectTurrets()
        {
            _simpleTurrets = new List<Turret>();
            foreach (Transform child in transform)
            {
                var tur = child.GetComponent<Turret>();
                if (tur != null)
                {
                    _simpleTurrets.Add(tur);
                }
            }
        }

        private void Start()
        {
            _angleOffset = new Vector3(0, 0, zAngleOffset);
            CollectTurrets();
        }
        
        private IEnumerator StageFire()
        {
            for (int i = 0; i < attackCount; i++)
            {
                _simpleTurrets.ForEach(e => e.Fire());
                transform.eulerAngles += _angleOffset;
                yield return new WaitForSeconds(timeAttackDelay);
            }
            StopCurrentAction();
        }

    }

}