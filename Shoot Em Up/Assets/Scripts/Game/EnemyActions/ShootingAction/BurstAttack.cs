﻿using Game.Gameplay.Turrets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Skills
{

    public class BurstAttack : EnemyAction
    {
        [SerializeField] float attackDelay;
        [SerializeField] int timesAttack;
        private List<Turret> _turrets;
        private int _currentCount;
        public override void Activate()
        {
            _currentCount = 0;
            Invoke("Fire", attackDelay);
        }

        void Fire()
        {
            foreach (var turret in _turrets)
            {
                turret.Fire();
            }
            _currentCount++;
            if (_currentCount < timesAttack)
            {
                Invoke("Fire", attackDelay);
            }
            else 
                StopCurrentAction();
        }
        
        // Start is called before the first frame update
        void Start()
        {
            _turrets = new List<Turret>();
            foreach (Transform child in transform)
            {
                var turret = child.gameObject.GetComponent<Turret>();
                if (turret != null)
                    _turrets.Add(turret);
            }
        }
    }

}