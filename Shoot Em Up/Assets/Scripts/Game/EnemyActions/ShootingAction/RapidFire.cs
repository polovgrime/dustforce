﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Gameplay.Turrets;
namespace Game.Gameplay.Skills
{

    public class RapidFire : EnemyAction
    {
        [SerializeField] float fireDelay;
        [SerializeField] int fireCount;
        [SerializeField] Turret turret;
        private int _iteration = 0;

        public override void Activate()
        {
            StartShooting();
        }
        // Start is called before the first frame update
        void Start()
        {
            transform.rotation = transform.parent.rotation;
            turret = Instantiate(turret, transform.position, transform.rotation, transform);
        }

        private void StartShooting()
        {
            _iteration++;
            if (_iteration <= fireCount && gameObject.activeSelf)
            {
                turret.Fire();
                Invoke("StartShooting", fireDelay);
            } 
            else 
            if (_iteration > fireCount)
            {
                StopCurrentAction();
            }
        }
        
    }
}
