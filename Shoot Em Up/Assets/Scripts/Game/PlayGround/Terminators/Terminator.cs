﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay
{
    public class Terminator : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<Player>() == null)
                Destroy(collision.gameObject);
        }
    }

}
