﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay.Visuals
{

    public class EnemyParticleHolder : MonoBehaviour
    {
        bool inUse = false;
        [SerializeField] float cleanDelay = 0.5f;
        public static EnemyParticleHolder instance = null;

        public Transform GetTransform()
        {
            return transform;
        }

        void Start()
        {
            inUse = true;
            StartCoroutine(CleanParticles());
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            } else
                Destroy(gameObject);
        }

        private void OnEnable()
        {
            if (inUse)
            {
                StartCoroutine(CleanParticles());

            }
        }

        IEnumerator CleanParticles()
        {
            foreach (Transform child in transform)
            {
                ParticleSystem ps = child.GetComponent<ParticleSystem>();
                if (ps != null)
                {
                    if (!ps.isPlaying || ps.isPaused)
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
            yield return new WaitForSeconds(cleanDelay);
            StartCoroutine(CleanParticles());
        }

        private void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }
    }

}