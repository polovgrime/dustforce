﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Gameplay
{

    public class BulletField : MonoBehaviour
    {
        private static BulletField instance = null;
        private void Start()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }
        
        public static Transform GetFieldTransform()
        {
            return instance.transform;
        }

        private void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }
    }

}