﻿using UnityEngine;
using UnityEngine.SceneManagement;
namespace Game.Menu
{
    public class MainMenu : MonoBehaviour
    {
        public void LoadGame()
        {
            SceneManager.LoadScene("MainGameScene");
        }
    }
}

