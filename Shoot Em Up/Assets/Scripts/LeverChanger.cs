﻿using Game.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LeverChanger : MonoBehaviour
{
    [SerializeField] string _currentLevel;
    [SerializeField] string _nextLevel;
    // Start is called before the first frame update
    void Start()
    {
        Player.instance.PlayerDied += OnPlayerDied;
        Boss.BossDied += OnBossDied;
    }

    private void OnPlayerDied()
    {
        SceneManager.LoadScene(_currentLevel);

    }

    private void OnBossDied()
    {
        SceneManager.LoadScene(_currentLevel);
    }

    private void OnDestroy()
    {
        Boss.BossDied -= OnBossDied;
    }
}
